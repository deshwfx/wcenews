<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wcnews');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/i]9O.Jw^wHsig4yk8)2r3bHaVuV;>vYVMhW-=z7ow|aT>M`8||ZTRv)iBO`h@{<');
define('SECURE_AUTH_KEY',  '+ByE8?qT_{Vc$i|_KonE(;=(G*}MAN=D=RbSQ2:+:>1C <ojV:182VLo,Ll0o]n1');
define('LOGGED_IN_KEY',    'nh^4,DMO+<Gs*NF)1sSa=*U.ztJY !*Q@}av]V$3KVvX3C(f_%(I<Uk7[M/fX~+r');
define('NONCE_KEY',        'hyi5;6u1(cm Ocdv{Ab%?o>@F5aj^ UaYF8BdD,qWb,;$i7}g_&<r&l*4Ei}[8,V');
define('AUTH_SALT',        'i!~XB^)Zrf3/|Ey:L]b:DdO71dS;^sq@VGVy ,ZhLi_S=%nm{RM`s2kqvpLAC:qq');
define('SECURE_AUTH_SALT', 'w8e5$#a[X}qP~PW0hWa(_/nz*[oEm5KYccF}|pT0Jy%hpcByL!b~+=g9O,x_%z^k');
define('LOGGED_IN_SALT',   '.J9=a>, j>:,;`Y&QZM!2Gd#HZLi2GnVe_x)Q#iH?2nHCP+iop#bUXdWK0PMF~Sw');
define('NONCE_SALT',       'yWTiHmFwc1j*xPFi]re9PD^f;1?+;5rXqfRY}lSlTCyQU;-+Wc{b<Tbz9OV~tC~c');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

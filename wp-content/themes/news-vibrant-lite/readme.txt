=== News Vibrant Lite ===

Contributors:       CodeVibrant Team
Requires at least:  WordPress 4.0
Tested up to:       WordPress 4.9
Version:            1.0.1
License:            GPLv3 or later
License URI:        http://www.gnu.org/licenses/gpl-3.0.html
Tags:               news, blog, grid-layout, custom-colors, one-column, two-columns, three-columns, left-sidebar, right-sidebar, custom-logo, footer-widgets, full-width-template, translation-ready


== Description ==

News Vibrant Lite is a child theme of News Vibrant modern magazine WordPress theme, with creative design and powerful features that lets you write articles and blog posts with ease. It uses the best clean SEO practices, responsive HTML5, and on top of that, it’s fast, simple, and easy to use.  Use the Customizer to add your own background, page layout, site width and more.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Copyright ==

News Vibrant is distributed under the terms of the GNU GPL

News Vibrant Lite WordPress Theme is child theme of News Vibrant Theme, Copyright 2018 CodeVibrant.
News Vibrant Lite is distributed under the terms of the GNU GPL

News Vibrant Lite bundles the following third-party resources:

    Font-Awesome Copyright 2012 Dave Gandy Font 
    Licenses: SIL OFL 1.1, MIT, CC BY 3.0
    Source: https://github.com/FortAwesome/Font-Awesome

	Screenshot Images
    Licenses: CCO Public Domain
    
	https://www.pexels.com/photo/administration-adult-anchor-anchorman-275496/
    https://www.pexels.com/photo/man-person-suit-united-states-of-america-2281/
    https://www.pexels.com/photo/close-up-digitaries-famous-heads-of-state-70550/
    https://www.pexels.com/photo/group-of-people-in-dress-suits-776615/
    https://www.pexels.com/photo/man-in-black-holding-phone-618613/
    https://www.pexels.com/photo/adult-business-choices-choosing-515169/
    https://www.pexels.com/photo/woman-in-white-t-shirt-holding-smartphone-in-front-of-laptop-914931/
    https://www.pexels.com/photo/man-doing-ice-skiing-on-snow-field-in-shallow-focus-photography-848618/
    https://www.pexels.com/photo/action-adult-athletes-battle-598689/
    https://www.pexels.com/photo/adult-athlete-body-bodybuilding-414029/
    https://www.pexels.com/photo/altitude-downhill-foggy-frost-306002/
    https://www.pexels.com/photo/actress-adult-attractive-audience-276046/
    https://www.pexels.com/photo/adult-audience-audio-cable-358129/
    https://www.pexels.com/photo/man-head-spin-159273/
    https://www.pexels.com/photo/active-adult-artist-ballerina-358010/

== Changelog ==

= 1.0.1 =
    * Fixed some changes according to review.

= 1.0.0 =
	* Submit on wp.org trac.